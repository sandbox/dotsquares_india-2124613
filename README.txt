
Admin Notification Module sends notification for order to admin and other emil id's filled in the settings form.

Built-in support for:
 - Drupal Coding Standards - http://drupal.org/node/318
 - Handle text in a secure fashion - http://drupal.org/node/28984
 - Converting 4.6.x modules to 4.7.x - http://drupal.org/node/22218
 - Converting 4.7.x modules to 5.x - http://drupal.org/node/64279

Installation
------------

Copy module to your module directory and then enable on the admin
modules page.  For inserting mail id's for notification, check admin/commerce/config/admin_notification

Author
------
Dotsquares

